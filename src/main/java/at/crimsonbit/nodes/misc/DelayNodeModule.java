package at.crimsonbit.nodes.misc;

import at.crimsonbit.nodesystem.nodebackend.module.NodeModule;
import at.crimsonbit.nodesystem.nodebackend.module.NodeRegistry;

public class DelayNodeModule extends NodeModule {

	@Override
	public void registerNodes(NodeRegistry registry) {
		registry.registerDefaultFactory(DelayNodes.DELAY_MS, DelayMSNode.class);
		registry.registerDefaultFactory(DelayNodes.DELAY_S, DelaySNode.class);
		registry.registerDefaultFactory(DelayNodes.DELAY_MIN, DelayMINNode.class);
		registry.registerDefaultFactory(DelayNodes.DELAY_H, DelayHNode.class);
	}

}
