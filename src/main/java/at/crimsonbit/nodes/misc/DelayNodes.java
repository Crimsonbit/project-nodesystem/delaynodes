package at.crimsonbit.nodes.misc;

import at.crimsonbit.nodesystem.nodebackend.INodeType;

public enum DelayNodes implements INodeType {

	DELAY_MS("Delay ms"), DELAY_S("Delay s"), DELAY_MIN("Delay min"), DELAY_H("Delay hour");

	private String name;

	DelayNodes(String n) {
		this.name = n;
	}

	@Override
	public String nodeName() {
		return name;
	}

}
