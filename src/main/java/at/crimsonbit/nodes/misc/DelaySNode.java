package at.crimsonbit.nodes.misc;

import at.crimsonbit.nodesystem.nodebackend.annotations.NodeField;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeInput;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeOutput;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;
import at.crimsonbit.nodesystem.nodebackend.util.Generic;
import at.crimsonbit.nodesystem.util.log.NSLogger;

public class DelaySNode extends AbstractNode {

	@NodeInput
	@NodeField
	long seconds;

	@NodeInput
	Object obj;

	@NodeOutput
	Generic output;

	@Override
	public void compute() {
		try {
			output = new Generic(obj);
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			NSLogger.getLogger("Delay Node").warn("Delay got interrupted!");
		}

	}

}
